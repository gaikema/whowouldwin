# Who Would Win?
Given any two inputs, says who would win in a fight.
Henceforth called WWW.

---

## Resources

### Text Analysis
* [Linguistic Data](https://reference.wolfram.com/language/guide/LinguisticData.html)
* [Text Analysis](https://reference.wolfram.com/language/guide/TextAnalysis.html)
* [Text Structure](https://reference.wolfram.com/language/ref/TextStructure.html)

### Forms
* http://www.codeproject.com/Articles/1078491/Creating-Forms-in-ASP-NET-MVC

### UI
* http://inspirobot.me/