# Change the stupid format Mathematica gives me into JSON.
# http://stackoverflow.com/a/4802004/5415895

from pyparsing import *

enclosed = Forward()
nestedParens = nestedExpr('(', ')', content=enclosed)
enclosed << (Word(alphas) | Word(nums) | ',' | nestedParens | '.')

f = open("test.txt", 'r')
data = f.read()

print enclosed.parseString(data).asList()

# TODO:
# Run this program from Mathematica and read the results.
# http://reference.wolfram.com/language/tutorial/ExternalPrograms.html