(* ::Package:: *)

(* ::Text:: *)
(*I'm not using a notebook because those are so large and I don't really care about formatting.*)


ClearAll["Global`*"]
SetDirectory[NotebookDirectory[]];
Get["test.wl"]


(* https://reference.wolfram.com/language/ref/TextStructure.html *)
TextStructure["85 angry bears.", "ConstituentString"]
str = ExportString[%,"Text"]
Export["test.txt", str]


(* End *)
