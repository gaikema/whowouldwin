﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WhoWouldWin.Models
{
	/// <summary>
	/// Model for the main form.
	/// https://docs.asp.net/en/latest/mvc/views/working-with-forms.html
	/// </summary>
	public class Input
	{
		[Required]
		public string LeftInput { get; set; }
		[Required]
		public string RightInput { get; set; }
	}
}

