﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace WhoWouldWin.Controllers.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			var mvcName = typeof(Controller).Assembly.GetName();
			var isMono = Type.GetType("Mono.Runtime") != null;

			ViewData ["Version"] = mvcName.Version.Major + "." + mvcName.Version.Minor;
			ViewData ["Runtime"] = isMono ? "Mono" : ".NET";

			return View();
		}

		/// <summary>
		/// Route for comparing two things.
		/// http://stackoverflow.com/a/14572008/5415895
		/// </summary>
		[HttpGet]
		public ActionResult Compare(string leftInput, string rightInput)
		{
			string response = "You want to compare " + leftInput + " and " + rightInput + ".";
			return Content(response);
		}
	}
}

