var webpack = require('webpack');
var path = require('path');

module.exports = {
	context: path.join(__dirname, "Scripts"),
	entry: {
		main: "./main.js", 
	},
	output: {
		path: path.join(__dirname, 'dist'),
		filename: "[name].min.js"
	},
	plugins: [
		new webpack.optimize.DedupePlugin(),
		new webpack.optimize.OccurenceOrderPlugin(),
		new webpack.optimize.UglifyJsPlugin({ mangle: false, sourcemap: false })
	],
	module: {
		loaders: [
			{		 
				test: /\.css$/,                 
				loader: ["style-loader", "css-loader"]
			},
			{
				test: /\.(jpe?g|gif|png)$/,
				loader: 'file-loader?emitFile=false&name=[path][name].[ext]'
			}
		]
	}
};